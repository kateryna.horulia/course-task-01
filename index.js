// TODO
const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const app = express();
const port = process.env.PORT || 56201;

app.use(express.text());
app.use(express.json());

app.post("/square", (req, res) => {
  const number = req.body;

  res.send({
    number: parseFloat(number),
    square: number * number
  });
});

app.post("/reverse", (req, res) => {
  const text = req.body;

  res.type('text/plain');
  res.send(text.split("").reverse().join(""));
});

app.get("/date/:year/:month/:day", (req, res) => {
  const {year, month, day} = req.params;

  const date = new Date(year, month - 1, day, 0, 0, 0);

  const weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  const weekDay =  weekdays[date.getDay()];

  const isLeapYear = (year & 3) == 0 && ((year % 25) != 0 || (year & 15) == 0);

  const currentDate = new Date();
  currentDate.setHours(0);
  currentDate.setMinutes(0);
  currentDate.setSeconds(0);
  currentDate.setMilliseconds(0)

  const difference = Math.ceil(Math.abs(currentDate - date) / (1000 * 60 * 60 * 24));

  res.send({
    weekDay,
    isLeapYear,
    difference
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})
